# Web Development Portfolio

#### This site includes all of my relevant projects from the Denver University Coding Bootcamp.

#### Made with: Bootstrap, CSS, Javascript, Font Awesome, and Google Fonts

#### The flex-box design for the landing page came from [Wes Bos](https://www.youtube.com/watch?v=9eif30i26jg&t=1s).

#### The background image for the projects section is from [Subtle Patterns](https://www.toptal.com/designers/subtlepatterns/). All other images are my own. 

Visit at https://zoe-gonzales.github.io/
